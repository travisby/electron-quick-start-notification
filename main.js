const {app, Notification} = require('electron');

app.on('ready', function () {
    if (Notification.isSupported()) {
        new Notification({title: "Hello", body: "World", silent: false}).show();
    } else {
        console.error("Notification is not supported");
    }
    app.quit();
});
