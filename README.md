# electron-quick-start

**Clone and run for a quick way to see Electron notifications in action.**

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://gitlab.com/travisby/electron-quick-start-notification
# Go into the repository
cd electron-quick-start-notification
# Install dependencies
npm install
# Run the app
npm start
```

This will launch an effectively "headless" app that will send a notification, and then quit.
If you see a notification, it worked.  If not, you may see:

```bash
$ npm start

> electron-quick-start-notification@1.0.0 start $CWD
> electron .

Fontconfig warning: "/etc/fonts/fonts.conf", line 100: unknown element "blank"

(electron:24050): libnotify-WARNING **: Failed to connect to proxy

(electron:24050): libnotify-WARNING **: Failed to connect to proxy

(electron:24050): libnotify-WARNING **: Failed to connect to proxy
ATTENTION: default value of option force_s3tc_enable overridden by environmen
$
```

If that's the case, the workaround we have right now is to start a dbus sessoin

```bash
$ npm start

> electron-quick-start-notification@1.0.0 start $CWD
> electron .

Fontconfig warning: "/etc/fonts/fonts.conf", line 100: unknown element "blank"
ATTENTION: default value of option force_s3tc_enable overridden by environment.
$
```

And afterwards you should see your notification.

Note: If you're using Linux Bash for Windows, [see this guide](https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/) or use `node` from the command prompt.

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
